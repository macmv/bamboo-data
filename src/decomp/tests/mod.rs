use crate::{
  decomp,
  decomp::{Class, Converter},
};
use rayon::prelude::*;
use std::{fs, path::PathBuf, process::Command};

mod tests;

pub struct Context {
  jvm:        &'static str,
  javac_path: PathBuf,
  tmp_dir:    PathBuf,
}

pub struct ClassFile {
  java_path:  PathBuf,
  class_path: PathBuf,
}

struct NoneConverter;

impl decomp::NotDecodeError for NoneConverter {}

impl Converter for NoneConverter {
  type Err = std::convert::Infallible;

  fn ver(&self) -> crate::Version {
    crate::Version::Release(crate::ReleaseVersion::new(8, 0))
  }

  fn has_class_file(&mut self, name: &str) -> bool {
    false
  }
  fn class_file(&mut self, name: &str) -> &[u8] {
    panic!("no such file: {name}")
  }
  fn class(&self, name: &str) -> String {
    name.into()
  }
  fn class_rev(&self, name: &str) -> String {
    name.into()
  }
  fn func(&mut self, _: &str, name: &str, _: &str) -> Result<String, std::convert::Infallible> {
    Ok(name.into())
  }
  fn field(&mut self, _: &str, name: &str) -> Result<String, std::convert::Infallible> {
    Ok(name.into())
  }
}

impl ClassFile {
  pub fn read_all(&self) -> Vec<u8> {
    fs::read(&self.class_path).unwrap()
  }
}

impl Context {
  pub fn new(jvm: &'static str) -> Self {
    let java_home = PathBuf::from("/usr/lib/jvm/").join(jvm);
    let javac_path = java_home.join("bin/javac");

    let tmp_dir = PathBuf::from(format!("/dev/shm/bamboo-data-{jvm}"));
    fs::create_dir_all(&tmp_dir).unwrap();

    Context { jvm, javac_path, tmp_dir }
  }

  #[track_caller]
  pub fn compile(&self, class_name: &str, src: &str) -> ClassFile {
    let java_path = self.tmp_dir.join(format!("{class_name}.java"));
    let class_path = self.tmp_dir.join(format!("{class_name}.class"));

    fs::write(&java_path, src).unwrap();
    let output = Command::new(&self.javac_path).arg(&java_path).output().unwrap();

    if output.status.success() {
      ClassFile { java_path, class_path }
    } else {
      println!("{}", String::from_utf8_lossy(&output.stderr));
      panic!("executing java failed");
    }
  }

  #[track_caller]
  pub fn decomp(&self, class_name: &str, src: &str) -> Class {
    let class = self.compile(class_name, src);

    decomp::class(&mut NoneConverter, &class.read_all(), |_, _, _, _| true)
      .unwrap_or_else(|e| panic!("failed to decompile class: {:?}", e))
  }

  #[track_caller]
  pub fn decomp_func(&self, class_name: &str, func_name: &str, src: &str) -> decomp::VarBlock {
    let class = self.decomp(class_name, src);

    let mut funcs =
      class.funcs.into_iter().filter(|(name, _, _)| name == func_name).collect::<Vec<_>>();
    assert_eq!(funcs.len(), 1);
    funcs.pop().unwrap().2
  }
}

#[test]
fn test_decompiler() {
  let jvms = ["java-8-openjdk", "java-11-openjdk", "java-17-openjdk"];

  jvms.into_par_iter().for_each(|jvm| {
    let ctx = Context::new(jvm);
    tests::run(ctx);
  });
}
