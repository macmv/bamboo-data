use super::Jar;
use anyhow::Result;
use serde::Serialize;

mod new;

#[derive(Debug, Clone, Serialize)]
pub struct Arg {
  name:      String,
  id:        u32,
  class:     String,
  has_extra: bool,
}

#[derive(Debug, Clone, Serialize)]
pub struct CommandDef {
  args: Vec<Arg>,
}

pub fn generate(jar: &mut Jar) -> Result<Option<impl Serialize>> {
  Ok(if jar.ver().maj() < 16 { None } else { Some(new::process(jar)?) })
}
