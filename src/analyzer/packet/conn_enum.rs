use noak::{
  error::DecodeError,
  reader::{
    attributes::{AttributeContent, RawInstruction},
    cpool::Item,
    Class,
  },
};

/// For 1.8-1.14.4, where packets are registered with a bunch of seperate
/// function calls in a static block.
pub fn read_old_conn_enum(bytes: &[u8]) -> Result<(Vec<String>, Vec<String>), DecodeError> {
  let mut class = Class::new(bytes)?;
  let mut clientbound = vec![];
  let mut serverbound = vec![];

  for method in class.methods()? {
    let method = method?;
    let pool = class.pool()?;
    let name = pool.retrieve(method.name())?.to_str().unwrap();
    if name == "<init>" {
      for a in method.attributes() {
        if let AttributeContent::Code(code) = a?.read_content(pool)? {
          let mut is_clientbound = true;
          for instr in code.raw_instructions() {
            let (_i, instr) = instr?;
            match instr {
              RawInstruction::GetStatic { index } => {
                // This is where a and b come from in that match:
                // ``` joined.srg
                // FD: fg/a net/minecraft/network/EnumPacketDirection/SERVERBOUND
                // FD: fg/b net/minecraft/network/EnumPacketDirection/CLIENTBOUND
                // ```
                match pool.retrieve(index)?.name_and_type.name.to_str().unwrap() {
                  "a" => is_clientbound = false,
                  "b" => is_clientbound = true,
                  s => panic!("unexpected static lookup for {}", s),
                }
              }
              RawInstruction::LdC { index } | RawInstruction::LdCW { index } => {
                match pool.get(index)? {
                  Item::Class(class) => {
                    if is_clientbound {
                      clientbound.push(pool.retrieve(class.name)?.to_str().unwrap().into());
                    } else {
                      serverbound.push(pool.retrieve(class.name)?.to_str().unwrap().into());
                    }
                  }
                  item => panic!("unexpected item {:?}", item),
                }
              }
              _ => {}
            }
          }
        }
      }
    }
  }

  Ok((clientbound, serverbound))
}

/// For 1.15.2 and up, where packets are registered with a bunch of chained
/// function calls, in the enum constructor.
pub fn read_new_conn_enum(bytes: &[u8]) -> Result<(Vec<String>, Vec<String>), DecodeError> {
  let mut class = Class::new(bytes)?;
  let this_class = class.this_class_name()?;
  let mut clientbound = vec![];
  let mut serverbound = vec![];

  for method in class.methods()? {
    let method = method?;
    let pool = class.pool()?;
    let name = pool.retrieve(method.name())?.to_str().unwrap();
    if name == "<clinit>" {
      for a in method.attributes() {
        if let AttributeContent::Code(code) = a?.read_content(pool)? {
          let mut in_play = false;
          let mut is_clientbound = false;
          for instr in code.raw_instructions() {
            let (_i, instr) = instr?;
            match instr {
              RawInstruction::GetStatic { index } => {
                // This is where a and b come from in that match:
                // ``` joined.srg
                // FD: fg/a net/minecraft/network/EnumPacketDirection/SERVERBOUND
                // FD: fg/b net/minecraft/network/EnumPacketDirection/CLIENTBOUND
                // ```
                let stat = pool.retrieve(index)?;
                if stat.class.name == this_class {
                  continue;
                }
                match stat.name_and_type.name.to_str().unwrap() {
                  "a" => is_clientbound = false,
                  "b" => is_clientbound = true,
                  s => panic!("unexpected static lookup for {}", s),
                }
              }
              RawInstruction::LdC { index } | RawInstruction::LdCW { index } => {
                match pool.get(index)? {
                  Item::Class(class) => {
                    if in_play {
                      if is_clientbound {
                        clientbound.push(pool.retrieve(class.name)?.to_str().unwrap().into());
                      } else {
                        serverbound.push(pool.retrieve(class.name)?.to_str().unwrap().into());
                      }
                    }
                  }
                  Item::String(text) => {
                    // This code block will be initialzing all packets. Before each packet
                    // initializing chain, there will be a string load. We need to make sure this is
                    // set to `false` after we are done reading PLAY packets.
                    in_play = pool.get(text.string)?.content.to_str().unwrap() == "PLAY";
                  }
                  item => panic!("unexpected item {:?}", item),
                }
              }
              _ => {}
            }
          }
        }
      }
    }
  }

  Ok((clientbound, serverbound))
}
