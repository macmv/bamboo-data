//! This is the second pass of the analyzer. It takes the java abstract syntax
//! tree, and conctxts that into a packet reader that is ctxy easy to conctxt to
//! Rust. It could also be conctxted into other languages, but it is targeted at
//! Rust for now.

use super::{Cond, Expr, Instr, Lit, Op, Range, Value, VarBlock, VarKind};
use crate::decomp;
use std::collections::HashMap;

type ReaderMap = HashMap<decomp::Descriptor, decomp::VarBlock>;

#[derive(Clone, Copy)]
struct Context<'a> {
  pub readers: &'a ReaderMap,
  pub var_map: &'a dyn Fn(usize) -> Expr,
}

pub fn pass(
  needs_super: bool,
  readers: &HashMap<decomp::Descriptor, decomp::VarBlock>,
) -> VarBlock {
  let decomp_block = if readers.len() == 1 {
    readers.iter().next().unwrap().1
  } else {
    readers
      .get(&decomp::Descriptor {
        args: vec![decomp::Type::Class("net/minecraft/network/PacketByteBuf".into())],
        ret:  decomp::Type::Void,
      })
      .unwrap_or_else(|| {
        readers
          .iter()
          .find(|(desc, _)| {
            desc.args == vec![decomp::Type::Class("net/minecraft/network/PacketByteBuf".into())]
          })
          .unwrap()
          .1
      })
  };
  pass_decomp_block(
    needs_super,
    decomp_block,
    Context { readers, var_map: &|var| Expr::new(Value::Var(var)) },
  )
}

fn pass_decomp_block(needs_super: bool, decomp_block: &decomp::VarBlock, ctx: Context) -> VarBlock {
  let block = pass_instr(needs_super, &decomp_block.instr, ctx);

  VarBlock {
    vars: decomp_block
      .vars
      .iter()
      .map(|v| match v {
        decomp::VarKind::This => VarKind::This,
        decomp::VarKind::Arg(_) => VarKind::Arg,
        decomp::VarKind::Local => VarKind::Local,
      })
      .collect(),
    block,
  }
}

fn pass_instr(needs_super: bool, p: &[decomp::Instr], ctx: Context) -> Vec<Instr> {
  let mut res = Vec::with_capacity(p.len());
  for (_, instr) in p.iter().enumerate() {
    match instr.clone() {
      decomp::Instr::Loop(cond, body) => {
        if let Some(Instr::Let(_, Expr { initial: _, ops })) = res.last() {
          if ops.len() == 1 && ops[0] == Op::Add(Expr::lit(1)) {
            if let decomp::Cond::LT(lhs, rhs) = cond {
              res.pop(); // Remove the Let
              res.push(Instr::For(
                Expr::from_decomp(lhs, ctx).unwrap_initial().unwrap_var(),
                Range { min: Expr::lit(0), max: Expr::from_decomp(rhs.clone(), ctx) },
                pass_instr(needs_super, &body, ctx),
              ));
            } else {
              panic!("unknown conditional while parsing function");
            }
          } else if matches!(&cond, decomp::Cond::NE(_, rhs) if rhs.initial == decomp::Value::Lit(0))
          {
            let lhs = match cond {
              decomp::Cond::NE(lhs, _) => lhs,
              _ => panic!("invalid cond"),
            };
            let ops = lhs.ops.borrow();
            if ops.len() == 1
              && matches!(&ops[0], decomp::Op::Call(cls, func, _, _) if cls == "java/util/Iterator" && func == "hasNext")
            {
              if body.len() > 1 && matches!(&body[0], decomp::Instr::Assign(_, _)) {
                res.pop();
                // TODO: For loops over arrays
                /*
                res.push(Instr::For(
                  Expr::from_decomp(lhs, ctx).unwrap_initial().unwrap_var(),
                  Expr::from_decomp(iter, ctx),
                  pass_instr(needs_super, &body, ctx),
                ));
                */
              } else {
                panic!("invalid hasNext iterator");
              }
            }
          } else {
            panic!("unknown loop syntax while parsing function");
          }
        }
      }
      decomp::Instr::If(cond, when_true, when_false) => res.push(Instr::If(
        Cond::from_decomp(cond, ctx),
        pass_instr(needs_super, &when_true, ctx),
        pass_instr(needs_super, &when_false, ctx),
      )),
      decomp::Instr::Switch(val, blocks, def) => res.push(Instr::Switch(
        Expr::from_decomp(val, ctx),
        blocks
          .into_iter()
          .map(|(key, block)| (key, pass_instr(needs_super, &block, ctx)))
          .collect(),
        def.map(|block| pass_instr(needs_super, &block, ctx)),
      )),
      decomp::Instr::SetField(name, val) => res.push(Instr::Set(name, Expr::from_decomp(val, ctx))),
      decomp::Instr::Assign(var, item) => res.push(Instr::Let(var, Expr::from_decomp(item, ctx))),
      decomp::Instr::SetArr(var, idx, item) => res.push(Instr::SetArr(
        Expr::from_decomp(var, ctx),
        Expr::from_decomp(idx, ctx).unwrap_initial(),
        Expr::from_decomp(item, ctx),
      )),
      decomp::Instr::Super(_) => {
        if needs_super {
          res.push(Instr::Super)
        }
      }
      decomp::Instr::Expr(item) => {
        if let Some(expr) = Expr::from_decomp_instr(item, &mut res, ctx) {
          res.push(Instr::Expr(expr));
        }
      }
      decomp::Instr::Return(v) => {
        if let Some(expr) = Expr::from_decomp_instr(v, &mut res, ctx) {
          if !(expr.initial == Value::Null && expr.ops.is_empty()) {
            res.push(Instr::Return(Box::new(expr)));
          }
        }
      }
    }
  }
  res
}

impl Expr {
  pub fn lit(v: impl Into<Lit>) -> Self {
    Expr { initial: Value::Lit(v.into()), ops: vec![] }
  }
  pub fn new(v: impl Into<Value>) -> Self {
    Expr { initial: v.into(), ops: vec![] }
  }
}

impl Expr {
  fn from_decomp_instr(v: decomp::Item, instr: &mut Vec<Instr>, ctx: Context) -> Option<Self> {
    let mut expr = value_from_decomp(v.initial, ctx);
    let ops = v.ops.borrow().clone();
    let mut replace = false;
    for op in ops {
      let new_instr = expr.add_op(op, ctx);
      if !new_instr.is_empty() {
        replace = true;
      }
      instr.extend(new_instr);
    }
    if replace {
      None
    } else {
      Some(expr)
    }
  }
  fn from_decomp(v: decomp::Item, ctx: Context) -> Self {
    Expr::from_decomp_instr(v, &mut vec![], ctx).unwrap()
  }
}

fn value_from_decomp(v: decomp::Value, ctx: Context) -> Expr {
  Expr::new(match v {
    decomp::Value::Lit(v) => Value::Lit(Lit::Int(v)),
    decomp::Value::LitFloat(v) => Value::Lit(Lit::Float(v)),
    decomp::Value::String(v) => Value::Lit(Lit::String(v)),
    decomp::Value::Null => Value::Null,
    decomp::Value::Field(_, v) => Value::Field(v),
    decomp::Value::Array(len) => Value::Array(Box::new(Expr::from_decomp(*len, ctx))),
    decomp::Value::StaticCall(class, name, args) => {
      Value::CallStatic(class, name, args.into_iter().map(|a| Expr::from_decomp(a, ctx)).collect())
    }
    decomp::Value::StaticField(class, _ty, name) => Value::Static(class, name),
    decomp::Value::Var(n) => return (ctx.var_map)(n),
    decomp::Value::Class(name) => Value::New(name, vec![]),
    decomp::Value::MethodRef(class, name) => Value::MethodRef(class, name),
    decomp::Value::Closure(args, instr) => Value::Closure(
      args.into_iter().map(|a| Expr::from_decomp(a, ctx)).collect(),
      pass_decomp_block(false, &instr, ctx),
    ),
    _ => unimplemented!("value {:?}", v),
  })
}

fn read_other_constructor(
  desc: decomp::Descriptor,
  arg_values: Vec<Expr>,
  ctx: Context,
) -> Vec<Instr> {
  let block = pass_decomp_block(
    false,
    &ctx.readers[&desc],
    Context {
      readers: ctx.readers,
      var_map: &|var| arg_values.get(var - 1).cloned().unwrap_or_else(|| Expr::new(Value::Null)),
    },
  );
  block.block
}
impl Expr {
  fn add_op(&mut self, op: decomp::Op, ctx: Context) -> Vec<Instr> {
    self.ops.push(match op {
      decomp::Op::And(v) => Op::BitAnd(Expr::from_decomp(v, ctx)),
      decomp::Op::Or(v) => Op::BitOr(Expr::from_decomp(v, ctx)),
      decomp::Op::Add(v) => Op::Add(Expr::from_decomp(v, ctx)),
      decomp::Op::Div(v) => Op::Div(Expr::from_decomp(v, ctx)),
      decomp::Op::Mul(v) => Op::Mul(Expr::from_decomp(v, ctx)),
      decomp::Op::Shr(v) => Op::Shr(Expr::from_decomp(v, ctx)),
      decomp::Op::UShr(v) => Op::UShr(Expr::from_decomp(v, ctx)),
      decomp::Op::Shl(v) => Op::Shl(Expr::from_decomp(v, ctx)),
      decomp::Op::Len => Op::Len,
      decomp::Op::If(cond, new) => {
        Op::If(Box::new(Cond::from_decomp(cond, ctx)), Expr::from_decomp(new, ctx))
      }
      decomp::Op::Call(class, name, desc, call_args) => {
        if name == "<init>" {
          assert!(self.ops.is_empty(), "cannot call constructor on value with ops {:?}", self);
          if let Value::New(_, args) = &mut self.initial {
            call_args.into_iter().for_each(|a| args.push(Expr::from_decomp(a, ctx)));
            return vec![];
          } else if self.initial == Value::Var(0) {
            // We called `this(args)`
            return read_other_constructor(
              desc,
              call_args.into_iter().map(|a| Expr::from_decomp(a, ctx)).collect(),
              ctx,
            );
          } else if self.initial == Value::Null {
            return vec![];
          } else {
            panic!("cannot call constructor on non-class type {:?}", self);
          }
        } else {
          self.ops.push(Op::Call(
            class,
            name,
            call_args.into_iter().map(|v| Expr::from_decomp(v, ctx)).collect(),
          ));
          return vec![];
        }
      }
      decomp::Op::Get(item) => match Expr::from_decomp(item, ctx).initial {
        // Value::Call(Some(v), name, args) => {
        //   if name == "ordinal" {
        //     assert!(args.is_empty(), "ordinal shouldn't have any args");
        //     assert!(self.ops.is_empty(), "ordinal shouldn't have any ops {:?}", self);
        //     *self = *v;
        //     return;
        //   } else {
        //     Op::Idx(Expr::new(v).op(Op::Call(name, args)))
        //   }
        // }
        Value::Var(v) => Op::Idx(Expr::new(Value::Var(v))),
        Value::Field(name) => Op::Field(name),
        v => panic!("unexpected Get {:?}", v),
      },
      decomp::Op::Cast(ty) => Op::Cast(ty),
      _ => unimplemented!("op {:?}", op),
    });
    vec![]
  }
}
impl Cond {
  fn from_decomp(v: decomp::Cond, ctx: Context) -> Self {
    match v {
      decomp::Cond::EQ(lhs, rhs) => {
        Cond::Eq(Expr::from_decomp(lhs, ctx), Expr::from_decomp(rhs, ctx))
      }
      decomp::Cond::NE(lhs, rhs) => {
        Cond::Neq(Expr::from_decomp(lhs, ctx), Expr::from_decomp(rhs, ctx))
      }
      decomp::Cond::LT(lhs, rhs) => {
        Cond::Less(Expr::from_decomp(lhs, ctx), Expr::from_decomp(rhs, ctx))
      }
      decomp::Cond::GT(lhs, rhs) => {
        Cond::Greater(Expr::from_decomp(lhs, ctx), Expr::from_decomp(rhs, ctx))
      }
      decomp::Cond::LE(lhs, rhs) => {
        Cond::Lte(Expr::from_decomp(lhs, ctx), Expr::from_decomp(rhs, ctx))
      }
      decomp::Cond::GE(lhs, rhs) => {
        Cond::Gte(Expr::from_decomp(lhs, ctx), Expr::from_decomp(rhs, ctx))
      }
      decomp::Cond::Or(lhs, rhs) => {
        Cond::Or(Box::new(Cond::from_decomp(*lhs, ctx)), Box::new(Cond::from_decomp(*rhs, ctx)))
      }
    }
  }
}

impl From<i32> for Lit {
  fn from(v: i32) -> Self {
    Lit::Int(v)
  }
}
impl From<f32> for Lit {
  fn from(v: f32) -> Self {
    Lit::Float(v)
  }
}

impl Expr {
  #[track_caller]
  pub fn unwrap_initial(self) -> Value {
    assert!(self.ops.is_empty(), "cannot unwrap initial when operators are present");
    self.initial
  }
}
impl Value {
  #[track_caller]
  pub fn unwrap_var(self) -> usize {
    match self {
      Value::Var(v) => v,
      _ => panic!("not a variable: {:?}", self),
    }
  }
  #[track_caller]
  #[allow(unused)]
  pub fn unwrap_lit(self) -> Lit {
    match self {
      Value::Lit(v) => v,
      _ => panic!("not a literal: {:?}", self),
    }
  }
  #[track_caller]
  #[allow(unused)]
  pub fn unwrap_call_static(self) -> (String, String, Vec<Expr>) {
    match self {
      Value::CallStatic(class, name, args) => (class, name, args),
      _ => panic!("not a static call: {:?}", self),
    }
  }
}
impl Lit {
  #[track_caller]
  #[allow(unused)]
  pub fn unwrap_int(self) -> i32 {
    match self {
      Lit::Int(v) => v,
      _ => panic!("not an int: {:?}", self),
    }
  }
}
